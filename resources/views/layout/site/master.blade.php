<!doctype html>
<html lang="en">
    @include('layout.site.blocks.head')
    <body>
        @include('layout.site.blocks.menu')
        @yield('content')
        @include('layout.site.blocks.footer')
    </body>
    @include('layout.site.blocks.script')
</html>