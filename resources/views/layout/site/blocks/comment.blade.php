<div id="carouselExampleIndicators" class="carousel slide my-carousel my-carousel" data-ride="carousel">
    <div class="container">		    
        <h1>تجربه کسانی که به ما اعتماد کردنند</h1>
        <div class="carousel-inner" role="listbox" id="quote-carousel">
            <div class="carousel-item active" >
                <h4>عباس الفتی</h4>
                <h6>مشاور کسب و کار</h6>
                <h5>برای راه اندازی یک سایت اینترنتی از مشاوره تاپمو کمک گرفتم و در حال حاضر یک استارتاپ موفق راه اندازی کرده ام. و همچنان برای اداره استارتاپم به مشورت هام ادامه میدهم</h5>
                <img src="{{asset('assets/site/image/1.png')}}" alt="">
            </div>
            <div class="carousel-item " >
                <h4>عباس الفتی</h4>
                <h6>مشاور کسب و کار</h6>
                <h5>برای راه اندازی یک سایت اینترنتی از مشاوره تاپمو کمک گرفتم و در حال حاضر یک استارتاپ موفق راه اندازی کرده ام. و همچنان برای اداره استارتاپم به مشورت هام ادامه میدهم</h5>
                <img src="{{asset('assets/site/image/1.png')}}" alt="">
            </div>
            <div class="carousel-item " >
                <h4>عباس الفتی</h4>
                <h6>مشاور کسب و کار</h6>
                <h5>برای راه اندازی یک سایت اینترنتی از مشاوره تاپمو کمک گرفتم و در حال حاضر یک استارتاپ موفق راه اندازی کرده ام. و همچنان برای اداره استارتاپم به مشورت هام ادامه میدهم</h5>
                <img src="{{asset('assets/site/image/1.png')}}" alt="">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <br>
        <br>
        <ol class="carousel-indicators">
            <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#quote-carousel" data-slide-to="1"></li>
            <li data-target="#quote-carousel" data-slide-to="2"></li>
        </ol>
    </div>
</div>
<script src="{{asset('assets/site/js/jquery-comment-3.3.1.slim.min.js')}}"></script>
<script src="{{asset('assets/site/js/bootstrap-comment.min.js')}}"></script>