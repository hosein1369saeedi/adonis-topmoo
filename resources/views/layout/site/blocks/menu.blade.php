<div class="menu top-nav w-100">
    <div class="container">
        <div class="row">
            <!-- start logo -->
            <div class="col-md-2 col-xs-10">
                <div class="logo">
                    <a href="{{url('/')}}">
                        <img class="logo" src="{{asset('assets/site/image/logo.png')}}" alt="">
                    </a>
                </div>
            </div>
            <!-- end logo -->
            <div class="col-md-10 col-xs-2">
                <!-- start nav -->
                <nav class="nav float-left w-100">
                    <div class="menu-area w-100">
                        <div class="row">
                            <nav class="navbar navbar-light navbar-expand-lg mainmenu">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="fa fa-bars" aria-hidden="true"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="active">
                                            <a href="{{url('/')}}">خانه <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                خدمات مشاوره
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right text-right" aria-labelledby="navbarDropdown">
                                                <li class="dropdown">
                                                    <a class="dropdown-toggle black" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        مشاوره تحصیلی و کسب و کار
                                                        <i class="fa fa-angle-left only-desktop" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right text-right" aria-labelledby="navbarDropdown">
                                                        <li><a href="{{url('services')}}" class="black">مشاوره راه اندازی مشاوره کسب وکار</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره انتخاب رشته تحصیلی</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره استخدام</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره کسب و کار اینترنتی</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره اینستاگرام</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره تلگرام</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-toggle black" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        مشاوره روانشناسی
                                                        <i class="fa fa-angle-left only-desktop" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right text-right" aria-labelledby="navbarDropdown">
                                                        <li><a href="{{url('services')}}" class="black">مشاوره خانواده</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره ازدواج</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره جنسی</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره فردی</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره افسردگی</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره کودک و نوجوان</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره روانشناسی آنلاین</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-toggle black" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        مشاوره اعتیاد
                                                        <i class="fa fa-angle-left only-desktop" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right text-right" aria-labelledby="navbarDropdown">
                                                        <li><a href="{{url('services')}}" class="black">مشاوره اعتیاد رفتاری</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره ترک اعتیاد</a></li>
                                                        <li><a href="{{url('services')}}" class="black">مشاوره اعتیاد کودک و نوجوان</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="{{url('advisers')}}">مشاوران ما</a></li>
                                        <li><a href="{{url('contact-us')}}">تماس باما</a></li>
                                        <li><a href="{{url('about')}}">درباره ما</a></li>
                                        <li><a href="{{url('blog')}}">بلاگ</a></li>
                                        <li><a href="#register">ثبت نام</a></li>
                                        <li><a href="{{url('/login')}}" >ورود</a></li>
                                        <li><a href="">خروج</a></li>
                                        <li>
                                            <a href="">
                                                <img src="{{asset('assets/site/image/user.png')}}" class="w-10" alt="">
                                                    نام و نام خانوادگی
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </nav>
                <nav class="menu-mobile">
                    <div id="mySidenav" class="sidenav">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <a href="index.php">
                            نام و نام خانوادگی
                            <img src="{{asset('assets/site/image/user.png')}}" class="w-10" alt="">
                        </a>
                        <a href="{{url('/')}}">خانه</a>
                        <a href="{{url('/')}}">خدمات مشاوره</a>
                        <a href="{{url('advisers')}}">مشاوران ما</a>
                        <a href="{{url('contact-us')}}">تماس با ما</a>
                        <a href="{{url('about-us')}}">درباره ما</a>
                        <a href="{{url('blog')}}">وبلاگ</a>
                        <a href="#register">ثبت نام</a>
                        <a href="{{url('login')}}">ورود</a>
                    </div>
                    <span style="font-size:50px;cursor:pointer;margin-right: -20%;color: #fff;" onclick="openNav()">&#9776;</span>
                </nav>
                <!-- end nav -->
            </div>
        </div>
    </div>
</div>