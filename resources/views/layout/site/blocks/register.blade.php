<section>
    <div class="banner" id="register">
        <div class="row boxbanner">
            <div class="parallax">
                <div class="row">
                    <div class="col-md-6 bannerright">
                        <div class="bannertitle">
                            <h3 class="panel-title">ثبت نام کاربر</h3>
                        </div>
                        <div class="container">
                            <div class="row centered-form">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <form role="form">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="نام">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="نام خانوادگی">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="email" name="email" id="email" class="form-control input-sm" placeholder="موبایل">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="password" name="password" id="password" class="form-control input-sm" placeholder="رمز">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="تکرار رمز">
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6 a">
                                                        <div class="mycheckbox">
                                                            <input class="mycheckbox" type="checkbox">
                                                            <label for="myCheckbox">پذیرش قوانین و مقررات</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a href="#" class="btn btn-xs bt-right">
                                                            مشاهده قوانین
                                                        </a>
                                                    </div>
                                                    <div class="normal-icon col-md-12">
                                                        <a href="#" class="btn btn-xs btn-login">
                                                        ثبت نام
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 bannerleft">
                        <div class="bannertitle">
                            <h3 class="panel-title">ثبت نام مشاور</h3>
                        </div>
                        <div class="container">
                            <div class="row centered-form">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <form role="form">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="نام" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="نام خانوادگی">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="email" name="email" id="email" class="form-control input-sm" placeholder="موبایل">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="password" name="password" id="password" class="form-control input-sm" placeholder="رمز">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="تکرار رمز">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row"></div>
                                                <br>
                                                <br>
                                                <div class="row">
                                                <div class="col-md-6 a">
                                                    <div class="mycheckbox">
                                                        <input class="mycheckbox" type="checkbox">
                                                        <label for="myCheckbox">پذیرش قوانین و مقررات</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="#" class="btn btn-xs bt-left">
                                                    مشاهده قوانین
                                                    </a>
                                                </div>
                                                <div class="normal-icon col-md-12">
                                                    <a href="#" class="btn btn-xs btn-loginleft">
                                                        ثبت نام
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>		
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</section>
  