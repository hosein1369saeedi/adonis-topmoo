<section>
    <div class="application">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h3>اپلیکیشن مشاوره تاپمو</h3>
                        <div class="row">
                            <div class="col-md text-center">
                                <img src="{{asset('assets/site/icon/gallery.png')}}" alt="">
                                <p>پرداخت و شارژ کیف پول</p>
                            </div>
                            <div class="col-md text-center">
                                <img src="{{asset('assets/site/icon/reserved.png')}}" alt="">
                                <p>امکان رزرو نوبت مشاوره</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md text-center">
                                <img src="{{asset('assets/site/icon/worldwide.png')}}" alt="">
                                <p>نمایش مشاوران نزدیک من</p>
                            </div>
                            <div class="col-md text-center">
                                <img src="{{asset('assets/site/icon/teamwork.png')}}" alt="">
                                <p>نمایش مشاوران آنلاین</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md text-center">
                                <img src="{{asset('assets/site/icon/avatar.png')}}" alt="">
                                <p>پروفایل مشاوران</p>
                            </div>
                            <div class="col-md text-center">
                                <img src="{{asset('assets/site/icon/avatar.png')}}" alt="">
                                <p>پروفایل کاربران</p>
                            </div>
                        </div>
                        <div class="row app">
                            <div class="col-md-6 text-center">
                                <a href="http://topmo.ir/">
                                    <img src="{{asset('assets/site/image/googleplay.png')}}" alt="">
                                </a> 
                            </div>
                            <div class="col-md-6 text-center">
                                <a href="http://topmo.ir/">
                                    <img src="{{asset('assets/site/image/appstore.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-4 text-center">
                        <img class="appimg" src="{{asset('assets/site/image/app-1.png')}}" alt="">    
                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>