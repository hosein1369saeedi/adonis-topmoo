<div class="numberplugin">
    <div class="row">
        <br/>
        <div class="col text-center">
            <h1>افتخارات ما</h1>
        </div>	    
    </div>
    <div class="row text-center">
        <div class="col">
            <div class="counter">
            <img src="{{asset('assets/site/icon/call.png')}}" alt="">
            <h2 class="timer count-title count-number" data-to="124" data-speed="1500"></h2>
            <p class="count-text ">پاسخگویی در هر روز</p>
            </div>
        </div>
        <div class="col">
            <div class="counter">
            <img src="{{asset('assets/site/icon/smile.png')}}" alt="">
            <h2 class="timer count-title count-number" data-to="140" data-speed="1500"></h2>
            <p class="count-text ">درمانجویان خوشحال</p>
            </div>
        </div>
        <div class="col">
            <div class="counter">
            <img src="{{asset('assets/site/icon/free.png')}}" alt="">
            <h2 class="timer count-title count-number" data-to="130" data-speed="1500"></h2>
            <p class="count-text ">مشاوره های رایگان </p>
            </div>
        </div>
        <div class="col">
            <div class="counter">
            <img src="{{asset('assets/site/icon/psychologist.png')}}" alt="">
            <h2 class="timer count-title count-number" data-to="15" data-speed="1500"></h2>
            <p class="count-text ">روانشناسان پاسخگو</p>
            </div>
        </div>
    </div>
</div>