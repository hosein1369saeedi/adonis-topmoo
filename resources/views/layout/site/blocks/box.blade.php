<section>
    <div class="boxs">
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <div class="imgBox">
                        <img src="{{asset('assets/site/image/map2.jpg')}}" alt="">
                    </div>
                    <div class="content">
                        <h2>جستجوی مشاوران نزدیک من</h2>
                        <p>چنانچه شما نیاز به مشاوره حضوری دارید و میخواهید مسافت کمتری را تا دفتر مشاور طی کنید ، وارد نقشه شده و بر اساس موقعیت فعلی خود مشاوران نزدیک را مشاهده نموده و برای شروع چت و یا رزرو نوبت مشاوره حضوری ، روی مشاور کلیک نمایید </p>
                        <h2><a class="btn btn-sm" href="#">مشاهده تقویم نوبت ها  </a></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="imgBox">
                        <img src="{{asset('assets/site/image/chat2.jpg')}}" alt="">
                    </div>
                    <div class="content">
                        <h2>درخواست مشاوره آنلاین</h2>
                        <p>پیشنهاد ما این است که برای یک مشارره بهینه و مفید تر از طریق رزرو اقدام به مشاوره نمایید ، چرا که از قبل هم شما و هم مشاور آمادگی بیشتری خواهد داشت، چنانچه شما عجله دارید ، می توانید یکی از مشاوران آنلاین ما را برای مشاوره انتخاب نمایید</p>
                        <h2><a class="btn btn-sm" href="#">مشاهده لیست آنلاین ها</a></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4"><div class="box">
                    <div class="imgBox">
                        <img src="{{asset('assets/site/image/chat2.jpg')}}" alt="">
                    </div>
                    <div class="content">
                        <h2>رزرو نوبت مشاوره </h2>
                        <p>چنانچه میخواهید در یک زمان مناسب نوبت مشاوره از روانشناسان ما بگیرید ، وارد تقویم شوید و زمان های خالی مشاور مورد نظر خود را رزرو نمایید و راس همان روز و ساعت برای شروع مشاوره به سایت و یا اپلیکیشن و یا تلفن و یا دفتر مشاور مراجعه نمایید </p>
                        <h2><a class="btn btn-sm" href="#">مشاهده مشاوران روی نقشه</a></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>