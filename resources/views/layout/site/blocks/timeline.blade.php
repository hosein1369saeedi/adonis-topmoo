<div class="process">
    <div class="container">
        <h4>مراحل مشاوره در تاپمو</h4>
        <div class="row">
            <div class="">
                <div class="main-timeline2">
                    <div class="timeline">
                        <span class="icon fa fa-sign-in"></span>
                        <a href="#" class="timeline-content">
                            <h3 class="title">۱-ثبت نام</h3>
                            <p class="description">
                                در مرحله اول لطفا در سایت ثبت نام نمایید.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-hand-pointer-o"></span>
                        <a href="#" class="timeline-content">
                            <h3 class="title">۲-انتخاب مشاور</h3>
                            <p class="description">
                                بر اساس نیاز خود لطفا مشاور مورد نظر خود را انتخاب کنید.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-mouse-pointer"></span>
                        <a href="#" class="timeline-content">
                            <h3 class="title">۳-رزرو</h3>
                            <p class="description">
                                ساعت مشاوره مورد نظر خود را رزرو و هزینه را پرداخت کنید.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-comments"></span>
                        <a href="#" class="timeline-content">
                            <h3 class="title">۴-شروع مشاوره</h3>
                            <p class="description">
                                مشاوره خود را شروع کنید.از مشاوره خود لذت برید.
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
</div>