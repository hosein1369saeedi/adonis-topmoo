<section>
    <div class="wrapper">
        <div class="row">
            <div class="col-md-6 boxleft">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/shield.png')}}" alt="">
                            <h6>امنیت و حفاظت</h6>
                            <p>در مشاوره آنلاین تاپمو، مطمئن باشید که با سرورهای امنیتی موجود بر روی سیستم   </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/like.png')}}" alt="">
                            <h6>دسترسی آسان به مشاور</h6>
                            <p> در اینجا دسترسی شما به مشاوران بدون رفتن از این سر شهر به آن سر شهر  </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/consultant.png')}}" alt="">
                            <h6>حق انتخاب</h6>
                            <p>در گروه تاپمو انتخاب مشاور با شماست و می توانید از مشاوران در زمینه های. </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/drop.png')}}" alt="">
                            <h6>همیشه و همه جا</h6>
                            <p>در مشاوره آنلاین تاپمو، مطمئن باشید که با سرورهای امنیتی موجود بر روی سیستم   </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/website.png')}}" alt="">
                            <h6>کاربری آسان</h6>
                            <p> در اینجا دسترسی شما به مشاوران بدون رفتن از این سر شهر به آن سر شهر و   </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/clock.png')}}" alt="">
                            <h6>ذخیره اطلاعات</h6>
                            <p>در گروه تاپمو انتخاب مشاور با شماست و می توانید از مشاوران در زمینه های. </p>
                        </div><div class="col-md-4">
                            <img src="{{asset('assets/site/icon/video.png')}}" alt="">
                            <h6>بررسی و بازبینی</h6>
                            <p>در مشاوره آنلاین تاپمو، مطمئن باشید که با سرورهای امنیتی موجود بر روی سیستم   </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/cart.png')}}" alt="">
                            <h6>کنترل کیفیت مشاوران</h6>
                            <p> در اینجا دسترسی شما به مشاوران بدون رفتن از این سر شهر به آن سر شهر و   </p>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('assets/site/icon/post.png')}}" alt="">    
                            <h6>رعایت انصاف</h6>
                            <p>در گروه تاپمو ببببتانتخاب مشاور با شماست و می توانید از مشاوران در زمیای. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 image">
                <img src="{{asset('assets/site/image/1212.png')}}" alt="">
            </div>
        </div>
    </div>
</section>