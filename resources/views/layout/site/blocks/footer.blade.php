<footer>
    <div class="footer">
        <div class="imgbackground">
            <div class="hover">
                <div class="container">
                    <div class="topfooter">
                        <div class="row">
                            <div class="col-md-4 col-xs-6">
                                <div class="row email">
                                    <div class="col-md-3">
                                        <img src="{{asset('assets/site/icon/mail.png')}}" alt="">
                                    </div>
                                    <div class="col-md-9">
                                        <h5>ایمیل</h5>
                                        <br>
                                        <h6>info@topmo.ir</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-6">
                                <div class="row call">
                                    <div class="col-md-3">
                                        <img src="{{asset('assets/site/icon/call2.png')}}" alt="">
                                    </div>
                                    <div class="col-md-9">
                                        <h5>شماره تلفن</h5>
                                        <br>
                                        <h6> 021-88638177 </h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row addres">
                                    <div class="col-md-3">
                                        <img src="{{asset('assets/site/icon/windows.png')}}" alt="">
                                    </div>
                                    <div class="col-md-9">
                                        <h5>آدرس</h5>
                                        <br>
                                        <p> امیرآباد شمالی، کوچه هفتم،ساختمان امیرآباد</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="bottomfooter">
                        <div class="row">
                            <div class="col-md-4">
                                <span>درباره ما</span>
                                <br>
                                <br>
                                <p>
                                    امروزه زندگی ما با دنیای مجازی گره خورده است و با پیشرفت علم و فناوری هر روز بیش از پیش به کاربرد تکنولوژی و ارتباطات از راه دور آگاه می شویم و خود را نیازمند استفاده از امکانات پیش رو می بینیم. بر همین اساس ما بر آن شدیم تا با اغتنام فرصت و بهره گیری از زمینه ها و امکانات وسیع موجود، متخصصان و مشاوران را در زمینه های مختلف روانشناسی، کسب و کارهای اینترنتی و غیره، در محیطی امن و کارآمد در فضای مجازی گرد هم آوریم و امکان ارتباط سریع و آسان با آنها را برای مردم عزیز کشورمان ایجاد کنیم.
                                </p>
                            </div>
                            <div class="col-md-2">
                                <span>لینک</span>
                                <br><br>
                                <ul>
                                    <li>
                                        <a href="index.php" type="button" class="btn2">خانه</a>
                                    </li>
                                    <li>
                                        <a href="services.php" type="button" class="btn2">خدمات مشاوره</a>
                                    </li>
                                    <li>
                                        <a href="advisers.php" type="button" class="btn2">مشاوران ما</a>
                                    </li>
                                    <li>
                                        <a href="contact-us.php" type="button" class="btn2">تماس با ما</a>
                                    </li>
                                    <li>
                                        <a href="about-us.php" type="button" class="btn2">درباره ما</a>
                                    </li>
                                    <li>
                                        <a href="blog.php" type="button" class="btn2">وبلاگ</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-2">
                                <span>نمادها</span>
                                <br><br>
                                <div class="footerimage">
                                    <div class="col-md-12">
                                        <div class="row image">
                                            <div class="col-md-6 col-xs-6">
                                                <img src="{{asset('assets/site/image/inamad.png')}}" alt="">
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <img src="{{asset('assets/site/image/inamad.png')}}" alt="">
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <img src="{{asset('assets/site/image/inamad.png')}}" alt="">
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <img src="{{asset('assets/site/image/inamad.png')}}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 loginfooter text-center">
                                <a href="index.php">
                                    <img src="{{asset('assets/site/image/logo-footer.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button onclick="topFunction()" id="myBtn" title="Go to top" style="display: block;">
            <img src="{{asset('assets/site/image/chevron.png')}}" alt="">
        </button>
    </div>
</footer>

<script>
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }
    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>    