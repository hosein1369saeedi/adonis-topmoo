@extends('layout.site.master')
@section('content')

	@include('layout.site.blocks.headerinternal')
        
    <div class="breadcrumbblog container">
        <nav class="breadcrumb pt-3">
            <a class="breadcrumb-item" href="index.php">خانه</a>
            <a class="breadcrumb-item" href="#"> تماس باما</a>
        </nav>
        <div class="contact p-5 mb-3">
            <section id="contact" class="parallax-section">
                <div class="row">
                    <div class="col-md-7 col-sm-10 pr-0">
                        <div class="fadeInUp section-title border-bottom" data-wow-delay="0.2s">
                            <h3>فرم تماس باما</h3>
                        </div>
                        <div class="wow fadeInUp mt-1" data-wow-delay="0.4s">
                            <form id="contact-form" action="#" method="get">
                                <div class="col-md-6 col-sm-6 pr-0">
                                    <input type="text" class="form-control" name="name" placeholder="نام و نام خانوادگی" required="">
                                </div>
                                <div class="col-md-6 col-sm-6 pr-0">
                                    <input type="email" class="form-control" name="email" placeholder="ایمیل" required="">
                                </div>
                                <div class="col-md-6 col-sm-6 pr-0">
                                    <input id="title" name="title" type="text" placeholder="موضوع" class="form-control">
                                </div>
                                <div class="col-md-12 col-sm-12 pr-0">
                                    <textarea class="form-control" rows="5" name="message" placeholder="پیام" required=""></textarea>
                                </div>
                                <div class="col-md-4 col-sm-6 pr-0 float-left">
                                    <button id="submit" type="submit" class="form-control white-text mt-0" name="submit">ارسال پیام</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-8 pl-0">
                        <div class="section-title border-bottom">
                            <h3>راه های تماس</h3>
                        </div>
                        <div class="wow fadeInUp contact-info mt-1" data-wow-delay="0.4s">
                            <p class="border-bottom"><i class="fa fa-map-marker"></i> <a>تهران، خیابان ولیعصر، نرسیده به خیابان طالقانی، کوچه رحیم زاده، پلاک 12</a></p>
                            <p class="border-bottom"><i class="fa fa-phone"></i> <a href="tel:021-66419139">021-66419139</a></p>
                            <p class="border-bottom"><i class="fa fa-comment"></i> <a href="mailto:info@company.com">info@topmo.ir</a></p>
                            <p class="border-bottom"><i class="fa fa-telegram"></i> <a href="">topmoir</a></p>
                            <p class="border-bottom"><i class="fa fa-instagram"></i> <a href="">topmoshaver</a></p>
                        </div>
                    </div>
                </div>
            </section>
            <hr>
            <div class="col-md-12 p-0">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12947.860112261504!2d51.3816411!3d35.7762354!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd2c06bece140dc48!2srahweb!5e0!3m2!1sen!2s!4v1562602387043!5m2!1sen!2s" style="border:0" allowfullscreen="" width="100%" height="450" frameborder="0"></iframe>
            </div>
        </div>
    </div>
        
    @include('layout.site.blocks.register')
@endsection