@extends('layout.site.master')
@section('content')

	@include('layout.site.blocks.headerinternal')

  <div class="breadcrumbblog container">
    <nav class="breadcrumb pt-3">
      <a class="breadcrumb-item" href="#">خانه</a>
      <a class="breadcrumb-item" href="#">مقالات</a>
      <a class="breadcrumb-item" href="#">ادامه مطلب</a>
    </nav>
    <div class="row py-5 ltr">
      <div class="col-md-3 sidebar pl-0 float-left">
        <div class="sidetitle pb-3">
          <h3>مقالات</h3>
        </div>
        <div class="sideblog">
          <ul class="p-0 m-0 d-inline">
            <li>
              <a class="row mt-2 border-bottom hover">
                <div class="col-md-4 pl-0">
                  <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100"/>
                </div>
                <div class="col-md-8 text-right pt-1 pr-1">
                  <h5 class="m-0 topmocolor">تیتر مقاله</h5>
                  <p class="m-0 note">تست</p>
                </div>
              </a>
            </li>
            <li>
              <a class="row mt-2 border-bottom hover">
                <div class="col-md-4 pl-0">
                  <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100"/>
                </div>
                <div class="col-md-8 text-right pt-1 pr-1">
                  <h5 class="m-0 topmocolor">تیتر مقاله</h5>
                  <p class="m-0 note">تست</p>
                </div>
              </a>
            </li>
            <li>
              <a class="row mt-2 border-bottom hover">
                <div class="col-md-4 pl-0">
                    <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100"/>
                </div>
                <div class="col-md-8 text-right pt-1 pr-1">
                  <h5 class="m-0 topmocolor">تیتر مقاله</h5>
                  <p class="m-0 note">تست</p>
                </div>
              </a>
            </li>
            <li>
              <a class="row mt-2 border-bottom hover">
                <div class="col-md-4 pl-0">
                  <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100"/>
                </div>
                <div class="col-md-8 text-right pt-1 pr-1">
                  <h5 class="m-0 topmocolor">تیتر مقاله</h5>
                  <p class="m-0 note">تست</p>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-9 detail pr-0 float-right">
        <div class="pb-3">
          <h3> فاصله زيبايى تا اختلالات روانى</h3>
        </div>
        <div class="row img rtl">
          <div class="col-md-6 pr-0">
            <img src="{{asset('assets/site/image/4205.jpg')}}"/>
          </div>
          <div class="col-md-6 pl-0">
            <h4 class="border-bottom pb-2 text-right">انتخاب همسر</h4>
            <p class="text-justify lh">
              برای رسیدن به آرامش و ثبات در زندگی به همدم و همراهی نیاز داریم که ما را بفهمد و شخصیت و منشی که داریم را دوست داشته باشد. شاید فکر کنید بعد از ازدواج درست می شود یا بدتر از آن فکر کنید بعدا درستش می کنم، اما این بدترین اشتباهی است که می توانید مرتکب شوید.
            </p>
            <p class="text-justify lh">
              انتخاب یک فرد برای ازدواج اصول و قواعد خاصی دارد که نمیتوان آنها را نادیده گرفت. هر کسی همانطور که چهره ای مخصوص به خود دارد، به همان اندازه هم شخصیتی مخصوص به خود دارد که متشکل از عوامل زیادی مانند ژنتیک و وراثت، تربیت و محیط رشد، جامعه و فرهنگ و خیلی موارد کوچک و بزرگ دیگر میشود.
            </p>
            <p class="text-justify lh">
              وقتی کسی را برای ادامه ی مسیر زندگی خودتان انتخاب می کنید، این فکر مخرب که بعدا او را درست میکنم یا بعدا فلان اخلاق او درست می شود از سرتان بیرون کنید. کسی در اینجا خراب نیست که قرار به درست شدنش باشد. شاید این فرد به دلیل داشتن چنین اخلاقی برای شما مناسب نباشد ولی این دلیلی بر درست یا غلط بودن او یا شما نیست.
            </p>
          </div>
        </div>
        <div class="py-1 rtl">
          <p>اختلال خوردن .بی اشتهایی پر اشتهایی پرخوری عصبی  :بیماری به نام آنورکسیا که علیرغم لاغر بودن فرد تصور می کند چاق است .بیماری  دیگر به نام بولیمیا است برخلاف افراد آنروکسیک غذا نمی خورند ولی متاسفانه به اشتباه تصور می‌کنند خیلی هوشمندانه می‌توانند کالری وارد شده بدنشان را خارج کنند اما آسیب های جبران ناپذیر بدنشان می زنند. اگر فردی در اطراف شما دچار این اختلالات است حواستان باشد که این افراد احتمالاً در حالت انکار به سر می‌برند و هر کسی که به مشکلشان اشاره بکند رو به سرعت از خودشان دور می‌کنند پس اوایل باید به صورت نامحسوس به آنها نزدیک شوید و کم‌کم حمایت خودتان را نسبت به آنها با احترام نشان بدهید طوری که احساس نکنم مورد قضاوت قرار می‌گیرند و در آن از جانب شما کنترل می‌شود فقط حمایت کنید. هرگز آنها را سرزنش نکنید و دقت کنید که درمان یک شبه اتفاق نمی افتد و صبورانه کنارشان باشد و در مورد این اختلال مطالعه کنید در طول پروسه درمانی بتوانید به صورتی از برگشتن اختلال جلوگیری کنید دنبال فهمیدن دلایل پس زمینه اختلال فرد باشید تا در بالا بردن اعتماد به نفسش و کمک به آن در مدیریت شرایط استرس‌زا کمک کنید طرز نگرش آنها نسبت به غذا شکل ایده آل اندام و ظاهر و وزن در طول زمان باید تغییر کند. یادتان نرود این مسئله یک جور لایف استایل شیوه زندگی نیست یک اختلال است یک بیماری که سلامت روان و جسم شما را شدیداً به خطر می اندازد و شما باید بدانید بهونه و توجیه پذیرش و احساساتی که منشاء این رفتارها شدن را درک کنید زندگی سالم و شاد تنها با برداشتن همین یک قدم شروع می شود و بعد شما ادامه می دهید برای برداشتن قدم‌های بهترشما احتیاج به تیم درمانی شامل متخصص تغذیه ،روانشناس و روانپزشک دارید متخصص تغذیه برای اینکه یک رژیم غذایی سالم برای شما بنویسد و نیاز بدن به یکسری مواد مغذی  به درستی برایتان توضیح بدهد روانشناسی  که به شما راهکارهایی برای بالا بردن اعتماد به نفس تان و شرایط استرس‌زا یاد بدهد و روان پزشک برای اینکه درصورت نیاز دارو هایی برای کنترل افسردگی اضطراب و وسواس برای شما تجویز کند. ولی مهمتر از این شمایی که باید کاپیتان تیم درمانی تان باشید و صدای خودتان را از صدای اختلال خودتان تشخیص بدهید امید داشتن تسلیم نشدن و با خود مهربان بودن یکی از مهمترین مشخصه های این کاپیتان است. بولیمیا و راهکارهایی برای افرادی که به این اختلال دچار هستند. افرادی که پرخوری می کنند اما چون با وزن شان اشتغال ذهنی دارند با  شیوه ناسالم اجازه جذب کالری را نمی‌دهند استفراغ می کنند مسهل میخورند قرص های چربی سوز میخورند ورزش های بسیار شدید می کنند.</p>
        </div>
        <div class="social py-1 rtl">
          <ul class="p-0">
            <li>
              <a class="btn btn-outline-topmo" href="">
                <i class="fa fa-gratipay"></i>
              </a>
            </li>
            <li>
              <a class="btn btn-outline-topmo" href="">
                <i class="fa fa-google-plus"></i>
                <span>اشتراک در گوگل پلاس</span>
              </a>
            </li>
            <li>
              <a class="btn btn-outline-topmo" href="">
                <i class="fa fa-telegram"></i>
                <span>اشتراک در تلگرام</span>
              </a>
            </li>
            <li>
              <a class="btn btn-outline-topmo" href="">
                <i class="fa fa-facebook"></i>
                <span>اشتراک در فیسبوک</span>
              </a>
            </li>
            <li>
              <a class="btn btn-outline-topmo" href="">
                <i class="fa fa-instagram"></i>
                <span>اشتراک در اینستاگرام</span>
              </a>
            </li>
          </ul>
        </div>
        <hr>
        <div class="blogcomment">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
              <form id="contact-form" method="post" action="" role="form">
                <div class="controls rtl">
                  <p class="m-0 p-0">لطفا نظرات خود را راجب به این مقاله با ما در میان بگذارید.</p>
                  <p class="m-0 p-0 red">پر کردن فیلد های ستاره دار الزامی است</p>
                  <div class="row mt-4">
                    <div class="col-md-4 pr-0">
                      <div class="form-group">
                        <label for="form_name">نام <span class="red">*</span></label>
                        <input id="form_name" type="text" name="name" class="form-control" placeholder="نام خود را وارد کنید" required="required" data-error="لطفا فیلد را پر کنید ">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="col-md-4 pr-0">
                      <div class="form-group">
                        <label for="form_phone">شماره تلفن <span class="red">*</span></label>
                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="شماره تلفن خود را وارد کنید">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="col-md-4 pr-0">
                      <div class="form-group">
                        <label for="form_email">ایمیل <span class="red">*</span></label>
                        <input id="form_email" type="email" name="email" class="form-control" placeholder="ایمیل خود را وارد کنید" required="required" data-error="لطفا فیلد را پر کنید">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 pr-0">
                      <div class="form-group">
                        <label for="form_message">پیام <span class="red">*</span></label>
                        <textarea id="form_message" name="message" class="form-control" placeholder="پیام خود را وارد کنید" rows="4" required data-error="لطفا فیلد را پر کنید"></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <input type="submit" class="btn btn-success1 btn-send float-left" value="ارسال نظر">
                    </div>
                  </div>
                  <hr>
                  <div class="row mt-5">
                    <h4>نظرات کاربران</h4>
                    <div class="col-md-12 offset-md-3 col-sm-6 offset-sm-3 col-12 comments-main pt-4 pr-0">
                      <ul class="p-0">
                        <li>
                          <div class="row comments mb-2">
                            <div class="col-md-1 col-sm-2 col-3 text-right user-img p-2">
                              <img id="profile-photo" src="{{asset('assets/site/image/user.png')}}" class="rounded-circle w-100" />
                            </div>
                            <div class="col-md-11 col-sm-9 col-9 comment mb-2 p-3">
                              <h4 class="m-0">
                                <a href="#">علی رضایی</a>
                              </h4>
                              <time class="mr-3 topmocolor">ساعت پیش</time>
                              <a href="" class="float-left red">پاسخ</a>
                              <like></like>
                              <p class="mb-0">در تربیت دو فرزند نوجوان خود به مشکل شدیدی برخورده بودیم، در ۳ نوبت از مشاوران تاپمو توانستیم راهکارهای خوبی برای این مشکل پیدا کنیم، ممنونم واقعا </p>
                            </div>
                          </div>
                        </li>
                        <ul class="p-0">
                          <li>
                            <div class="row comments mb-2">
                              <div class="col-md-1 offset-md-2 col-sm-2 offset-sm-2 col-3 offset-1 text-right user-img p-2">
                                <img id="profile-photo" src="{{asset('assets/site/image/user.png')}}" class="rounded-circle w-100" />
                              </div>
                              <div class="col-md-9 col-sm-7 col-8 comment mb-2 p-3">
                                  <h4 class="m-0">
                                    <a href="#">تاپمو</a>
                                  </h4>
                                  <time class="mr-3 topmocolor">ساعت پیش</time>
                                  <a href="" class="float-left red">پاسخ</a>
                                  <like></like>
                                  <p class="mb-0">ممنون دوست عزیز</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </ul>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row light details">
    <div class="col-md-12">
      <section class="my-0">
        <div class="container">
          <div class="col-xl-12 border-bottom mb-2">
            <h2 class="text-right topmocolor">مقالات مرتبط</h2>
          </div>
          <div id="carouselSixColumn" class="carousel slide" data-ride="carousel">
            <!--  <ol class="carousel-indicators">
              <li data-target="#carouselSixColumn" data-slide-to="0" class="active"></li>
              <li data-target="#carouselSixColumn" data-slide-to="1"></li>
            </ol>  -->
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="row">
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row">
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-2 p-1">				
                    <div class="card">
                      <img src="{{asset('assets/site/image/1.jpg')}}" class="w-100">
                      <div class="card-body p-0 pt-3">
                        <h5 class="card-title text-center">مقاله</h5>
                        <a href="#" class="btn btn-outline-topmo w-100">Show</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselSixColumn" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselSixColumn" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </section>
    </div>
  </div>
  @include('layout.site.blocks.register')
@endsection