@extends('layout.site.master')
@section('content')

	@include('layout.site.blocks.headerinternal')
    <div class="breadcrumbblog container">
        <nav class="breadcrumb pt-3">
            <a class="breadcrumb-item" href="index.php">خانه</a>
            <a class="breadcrumb-item" href="#"> مشاوران ما</a>
        </nav>
        <div class="row advisers py-4">
            <!-- <div class="col-md-3 pr-0">
                <div class="topmocolor pb-3 text-right w-100">
                    <h3>مشاوره ها</h3>
                </div>
                <div id="accordion" class="card-header">
                    <div class="">
                        <div class="card-header p-2" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link p-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                مشاوره تحصیلی و کسب و کار
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body p-1">
                                <ul class="p-2 m-0">
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور خانواده</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور ازدواج</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">تست</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card-header p-2" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed p-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    مشاوره روانشناسی
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body p-1">
                                <ul class="p-2 m-0">
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور خانواده</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور ازدواج</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">تست</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card-header p-2" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed p-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                مشاوره اعتیاد
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body p-1">
                                <ul class="p-2 m-0">
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور خانواده</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور ازدواج</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">تست</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
            </div> -->
            <div class="col-md-12 p-0">
                <div class="row">
                    <!-- <div class="topmocolor pb-3 w-100">
                        <h3>مشاوران ما</h3>
                    </div> -->
                    <div class="card-header w-100 row">
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                        <div class="w-20 text-center p-1">
                            <a href="{{url('advisers-details')}}" class="row border rounded p-3">
                                <div class="img w-100 text-center">
                                    <img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
                                </div>
                                <div class="text w-100">
                                    <div class="rating py-3">
                                    <div class="star-ratings-sprite">
                                        <span style="width:10%" class="star-ratings-sprite-rating"></span>
                                    </div>
                                    </div>
                                    <h5 class="topmocolor">نام و نام خانوادگی</h5>
                                    <p class="note">مشاور ازدواج</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layout.site.blocks.register')
@endsection