@extends('layout.site.master')
@section('content')

	@include('layout.site.blocks.headerinternal')
	<div class="breadcrumbblog container">
		<nav class="breadcrumb pt-3">
			<a class="breadcrumb-item" href="index.php">خانه</a>
			<a class="breadcrumb-item" href="advisers"> مشاوران ما</a>
			<a class="breadcrumb-item" href="#"> پروفایل مشاور</a>
		</nav>
		<div class="row py-3 light">
			<div class="col-md-3 pr-0">
				<div class="person p-3 h-100">
					<div class="w-50 float-right pr-3">
						<img src="{{asset('assets/site/image/user.png')}}" class="w-60" alt="">
					</div>
					<div class="w-50 float-left pt-1">
						<h5>نام و نام خانوادگی</h5>
						<p class="m-0">مشاور خانواده</p>
						<p class="p-0">09123456789</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layout.site.blocks.register')
@endsection