@extends('layout.site.master')
@section('content')

    @include('layout.site.blocks.header')
    @include('layout.site.blocks.application')
    @include('layout.site.blocks.box')
    @include('layout.site.blocks.comment')
    @include('layout.site.blocks.wrapper')
    @include('layout.site.blocks.pluginnumber')
    @include('layout.site.blocks.register')
    @include('layout.site.blocks.timeline')
    
@endsection