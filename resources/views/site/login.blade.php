@extends('layout.site.master')
@section('content')

	@include('layout.site.blocks.headerinternal')
    <div class="breadcrumbblog container">
        <nav class="breadcrumb pt-3">
            <a class="breadcrumb-item" href="index.php">خانه</a>
            <a class="breadcrumb-item" href="#"> فرم ورود</a>
        </nav>
        <div class="row login">
            <div class="col-md-6 px-5">
                <form class="px-5 w-75 m-auto">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="شماره همراه" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="رمز عبور" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <div class="form-check mb-2 mr-sm-2">
                        <input class="form-check-input" type="checkbox" id="inlineFormCheck">
                        <label class="form-check-label" for="inlineFormCheck">
                        من را فراموش نکن
                        </label>
                    </div>
                    <button type="" class="btn btn-outline-info w-100 float-left">ورود</button>
                </form>
            </div>
        </div>
    </div>
    
@endsection