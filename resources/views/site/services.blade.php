@extends('layout.site.master')
@section('content')

	@include('layout.site.blocks.headerinternal')
        
    <div class="breadcrumbblog container">
        <nav class="breadcrumb pt-3">
            <a class="breadcrumb-item" href="index.php">خانه</a>
            <a class="breadcrumb-item" href="#"> مشاوران ما</a>
        </nav>
        <div class="row advisers py-4">
            <!-- <div class="col-md-3 pr-0">
                <div class="topmocolor pb-3 text-right w-100">
                    <h3>مشاوره ها</h3>
                </div>
                <div id="accordion" class="card-header">
                    <div class="">
                        <div class="card-header p-2" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link p-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                مشاوره تحصیلی و کسب و کار
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body p-1">
                                <ul class="p-2 m-0">
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور خانواده</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور ازدواج</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">تست</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card-header p-2" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed p-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    مشاوره روانشناسی
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body p-1">
                                <ul class="p-2 m-0">
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور خانواده</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور ازدواج</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">تست</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="card-header p-2" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed p-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                مشاوره اعتیاد
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body p-1">
                                <ul class="p-2 m-0">
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور خانواده</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">مشاور ازدواج</a>
                                    </li>
                                    <li class="w-100 py-2 border-bottom">
                                        <a href="" class="w-100">تست</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-md-12 p-0">
                <div class="row">
                    <div class="topmocolor pb-3 w-100">
                        <h3>مشاوره ازدواج</h3>
                    </div>
                    <div class="card-header w-100 row">
                        <div class="col-md-6 pr-0">
                            <img src="image/services/marrid2.jpg" class="w-100" alt="">
                        </div>
                        <div class="col-md-6 pl-0">
                            <h4 class="border-bottom pb-2 text-right">انتخاب همسر</h4>
                            <p class="text-justify lh">
                                برای رسیدن به آرامش و ثبات در زندگی به همدم و همراهی نیاز داریم که ما را بفهمد و شخصیت و منشی که داریم را دوست داشته باشد. شاید فکر کنید بعد از ازدواج درست می شود یا بدتر از آن فکر کنید بعدا درستش می کنم، اما این بدترین اشتباهی است که می توانید مرتکب شوید.
                            </p>
                            <p class="text-justify lh">
                                انتخاب یک فرد برای ازدواج اصول و قواعد خاصی دارد که نمیتوان آنها را نادیده گرفت. هر کسی همانطور که چهره ای مخصوص به خود دارد، به همان اندازه هم شخصیتی مخصوص به خود دارد که متشکل از عوامل زیادی مانند ژنتیک و وراثت، تربیت و محیط رشد، جامعه و فرهنگ و خیلی موارد کوچک و بزرگ دیگر میشود.
                            </p>
                            <p class="text-justify lh">
                                وقتی کسی را برای ادامه ی مسیر زندگی خودتان انتخاب می کنید، این فکر مخرب که بعدا او را درست میکنم یا بعدا فلان اخلاق او درست می شود از سرتان بیرون کنید. کسی در اینجا خراب نیست که قرار به درست شدنش باشد. شاید این فرد به دلیل داشتن چنین اخلاقی برای شما مناسب نباشد ولی این دلیلی بر درست یا غلط بودن او یا شما نیست.
                            </p>
                        </div>
                        <div class="col-md-12 p-0 pt-3">
                            <h4 class="border-bottom pb-2 text-right">مشاوره پیش از ازدواج</h4>
                            <p class="text-justify lh">
                                ما پیشنهاد میکنیم پیش از ازدواج به سراغ مشاوره بروید، مشاوری را انتخاب کنید که بتواند اخلاق هایتان را خوب بشناسد و به شما در شناخت بهتر خودتان کمک کند و همچنین در انتخاب همسری مناسب شما را راهنمایی کند. شاید فکر کنید حداقل خودتان را خوب می شناسید ولی شناختی که برای ازدواج نیاز دارید بسیار پیچیده تر و گسترده تر از چیزی ست که درباره خودتان می دانید و یا تصور می کنید که می دانید. مثلا اینکه چه رنگی را دوست دارید بی اهمیت نیست ولی سرنوشت ساز هم نیست ولی گاهی این نکته که چرا یک رنگ را بیشتر و خیلی زیاد دوست دارید، می تواند مهم شود و نشانی از روحیاتی خاص در درون شما باشد.
                            </p>
                            <p class="text-justify lh">
                                مشاوره آنلاین ازدواج شامل شرکت در جلساتی است که مشاور با توجه به شما تعداد آن را تعیین می کند. این جلسات شامل گفت و گو با مشاورآنلاین تاپمو و البته با فرد مورد نظرتان در حضور مشاور می شود و بعضی تمرین ها و آزمون ها در طی آنها اجرا خواهد شد.
                            </p>
                            <p class="text-justify lh">
                                و در نهایت پس از گذراندن چند جلسه مشاوره  با شخص انتخابی خود میتوانید به درستی تصمیم بگیرید که این فرد برای ازدواج با شما مناسب هست یا ممکن است در آینده به مشکلات جدی برخورد کنید
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layout.site.blocks.register')
@endsection